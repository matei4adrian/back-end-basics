class Crocodil {
    constructor(nume, prenume, varsta, email, telefon, activ) {
      this.nume = nume;
      this.prenume = prenume;
      this.varsta = varsta;
      this.email = email;
      this.telefon = telefon;
      this.activ = activ;
    }
  }
  
  module.exports = Crocodil;