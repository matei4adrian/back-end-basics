const express = require("express");
const sql = require("mysql");

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

let port = 8080;

app.listen(port, () => {
    console.log("Serveru merge pa " + port);
})

const connection = sql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    database: "crocos_klan"
})

connection.connect((err) => {
    if (err) throw err;
    console.log("Baza de date conectata!");

    const sqlQuery = "CREATE TABLE IF NOT EXISTS Crocodili(id INTEGER NOT NULL AUTO_INCREMENT, nume VARCHAR(30), prenume VARCHAR(30), varsta INTEGER, email VARCHAR(50), telefon VARCHAR(10), activ BOOLEAN, PRIMARY KEY(id))";
    connection.query(sqlQuery, err => {
        if (err) throw err;
        else
            console.log("Tabela Crocodili creata/existenta!");
    })
})

app.post("/crocodili", (req, res) => {
    const croco = {
        nume: req.body.nume,
        prenume: req.body.prenume,
        varsta: req.body.varsta,
        email: req.body.email,
        telefon: req.body.telefon,
        activ: req.body.activ,
    }

    let errors = [];

    //validari
    if (!croco.nume || !croco.prenume || !croco.varsta || !croco.email || !croco.telefon || croco.activ === undefined) {
        errors.push("Exista campuri necompletate!");
    }
    if (croco.varsta < 18) {
        errors.push("Ia bacu'!");
    }
    if (!croco.email.includes("@gmail.com") && !croco.email.includes("@yahoo.com")) {
        errors.push("Email incorect");
    }
    if (croco.telefon.length !== 10) {
        errors.push("Telefon incorect");
    }

    if (errors.length === 0) {
        try {
            const insertQuery = `INSERT INTO Crocodili(nume,prenume,varsta,email,telefon,activ)
         VALUES('${croco.nume}', '${croco.prenume}', '${croco.varsta}', '${croco.email}', '${croco.telefon}', '${croco.activ}')`;

            connection.query(insertQuery, err => {
                if (err) throw err;
                else {
                    console.log("Bine ai venit, rau ai nimerit");
                    res.status(201).send({ message: "Bine ai venit, rau ai nimerit" })
                }
            })
        } catch (err) {
            console.log("Server error");
            res.status(500).send(err);
        }
    }
    else {
        console.log("Eroare!");
        res.status(400).send(errors);
    }
})

app.get("/crocodili", (req, res) => {
    let select = '';
    if (req.query.activ) {
        select = `SELECT * FROM Crocodili WHERE activ = '${req.query.activ}'`;
    } else {
        select = "SELECT * FROM Crocodili";
    }
    connection.query(select, (err, result) => {
        if (err) throw err;
        res.status(200).send(result);
    })
})

app.delete("/crocodili/:id", (req, res) => {
    const sqlDelete = `DELETE FROM Crocodili WHERE id = '${req.params.id}'`;
    connection.query(sqlDelete, (err) => {
        if (err) throw (err);
        res.status(200).send({ message: "Crocodil disparut" })
    })
})

//TEMA: GET by id & PUT by id